#############################################################
#
# python-cherrypy
#
#############################################################

PYTHON_CHERRYPY_VERSION = 3.2.2
PYTHON_CHERRYPY_SOURCE = CherryPy-$(PYTHON_CHERRYPY_VERSION).tar.gz
PYTHON_CHERRYPY_SITE = http://download.cherrypy.org/cherrypy/$(PYTHON_CHERRYPY_VERSION)
PYTHON_CHERRYPY_SETUP_TYPE = setuptools

HOST_PYTHON_CHERRYPY_NEEDS_HOST_PYTHON = python2
HOST_PYTHON_CHERRYPY_DEPENDENCIES = 

$(eval $(python-package))
$(eval $(host-python-package))
