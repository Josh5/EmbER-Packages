################################################################################
#
# ember_par2
#
################################################################################

EMBER_PAR2_VERSION = v0.6.14
EMBER_PAR2_SITE = git@github.com:Parchive/par2cmdline.git
EMBER_PAR2_SITE_METHOD = git
EMBER_PAR2_INSTALL_STAGING = NO
EMBER_PAR2_INSTALL_TARGET = YES

EMBER_PAR2_AUTORECONF = YES

EMBER_PAR2_PK_NAME = "PAR2"
EMBER_PAR2_PK_VERSION = 0.6.14
EMBER_PAR2_PK_DESCRIPTION = "Produces par files for checksum verification of data integrity."
EMBER_PAR2_PK_DEPENDENCIES = ""
EMBER_PAR2_PK_REQUIREDFW = "2.0.0"
EMBER_PAR2_PK_DOWNLOAD = "https://bitbucket.org/ember-dev/ember-packages/raw/master/PACKAGES-armv7l/package-PAR2.tar.gz"

ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

ifeq ($(BR2_PREFER_STATIC_LIB),y)
EMBER_PAR2_CONF_OPTS += --enable-static --disable-shared
else
EMBER_PAR2_CONF_OPTS += --enable-shared --disable-static
endif

define EMBER_PAR2_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_PAR2_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'name = $(EMBER_PAR2_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'description = $(EMBER_PAR2_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'version = "$(EMBER_PAR2_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_PAR2_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_PAR2_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info
	echo 'download = $(EMBER_PAR2_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))/package_info

	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/bin
	$(INSTALL) -m 0755 -D $(@D)/par2 $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/bin/par2
	cp -rfv $(BR2_EXTERNAL)/package/ember_par2/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PAR2_PK_NAME))/Programs/$(call qstrip,$(EMBER_PAR2_PK_NAME))
endef

$(eval $(autotools-package))
