#############################################################
#
# python-openssl
#
#############################################################

# Keep at 0.13 for now untill we can confirm that no other packages will be broken by the bump
PYTHON_OPENSSL_VERSION = 0.13
PYTHON_OPENSSL_SOURCE  = pyOpenSSL-$(PYTHON_OPENSSL_VERSION).tar.gz
PYTHON_OPENSSL_SITE    = http://pypi.python.org/packages/source/p/pyOpenSSL
PYTHON_OPENSSL_SETUP_TYPE = distutils

HOST_PYTHON_OPENSSL_NEEDS_HOST_PYTHON = python2
HOST_PYTHON_OPENSSL_DEPENDENCIES = openssl

$(eval $(python-package))
