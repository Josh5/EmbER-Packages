#############################################################
#
# python-yenc
#
#############################################################

PYTHON_YENC_VERSION = 0.4.0
PYTHON_YENC_SOURCE  = yenc-$(PYTHON_YENC_VERSION).tar.gz
PYTHON_YENC_SITE    = http://www.golug.it/pub/yenc
PYTHON_YENC_SETUP_TYPE = distutils

PYTHON_YENC_DEPENDENCIES = python

$(eval $(python-package))
