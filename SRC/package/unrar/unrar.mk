#############################################################
#
# unrar
#
#############################################################

UNRAR_VERSION = 5.1.6
UNRAR_SOURCE = unrarsrc-$(UNRAR_VERSION).tar.gz
UNRAR_SITE = http://www.rarlab.com/rar
UNRAR_INSTALL_STAGING = YES


define UNRAR_BUILD_CMDS
	$(MAKE) CXX="$(TARGET_CXX)" CXXFLAGS="$(TARGET_CXXFLAGS)" DEFINES="-D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE" STRIP="$(TARGET_STRIP)" --makefile=makefile -C $(@D) all
endef

define UNRAR_INSTALL_STAGING_CMDS
	$(MAKE) DESTDIR="$(STAGING_DIR)/usr" --makefile=makefile -C $(@D) install
endef

define UNRAR_INSTALL_TARGET_CMDS
	$(MAKE) DESTDIR="$(TARGET_DIR)/usr" --makefile=makefile -C $(@D) install
endef

$(eval $(call generic-package))
