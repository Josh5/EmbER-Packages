################################################################################
#
# ember_nzbget
#
################################################################################

EMBER_NZBGET_VERSION = 16.2
EMBER_NZBGET_SOURCE = nzbget-$(EMBER_NZBGET_VERSION)-bin-linux.run
EMBER_NZBGET_SITE = http://github.com/nzbget/nzbget/releases/download/v${EMBER_NZBGET_VERSION}
EMBER_NZBGET_INSTALL_STAGING = YES

EMBER_NZBGET_PK_NAME = "NZBGet"
EMBER_NZBGET_PK_VERSION = $(EMBER_NZBGET_VERSION).0
EMBER_NZBGET_PK_DESCRIPTION = "NZBGet is an Usenet-client written in C++ and designed with performance in mind to achieve maximum download speed by using very little system resources. (Default Port: 6789)"
EMBER_NZBGET_PK_DEPENDENCIES = ""
EMBER_NZBGET_PK_REQUIREDFW = "2.0.0"
EMBER_NZBGET_PK_DOWNLOAD = "https://bitbucket.org/ember-dev/ember-packages/raw/master/PACKAGES-armv7l/package-NZBGet.tar.gz"
EMBER_NZBGET_PK_INITSCRIPT = "PNZBGet"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_NZBGET_EXTRACT_CMDS
	cp $(DL_DIR)/$(EMBER_NZBGET_SOURCE) $(@D)
endef

define EMBER_NZBGET_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_NZBGET_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'name = $(EMBER_NZBGET_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'description = $(EMBER_NZBGET_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'version = "$(EMBER_NZBGET_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_NZBGET_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_NZBGET_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info
	echo 'download = $(EMBER_NZBGET_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/package_info

	$(INSTALL) -m 0755 -D $(@D)/$(EMBER_NZBGET_SOURCE) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))/$(EMBER_NZBGET_SOURCE)
	cp -rfv $(BR2_EXTERNAL)/package/ember_nzbget/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Programs/$(call qstrip,$(EMBER_NZBGET_PK_NAME))
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL)/package/ember_nzbget/$(call qstrip,$(EMBER_NZBGET_PK_INITSCRIPT)) $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_NZBGET_PK_NAME))/Run/$(call qstrip,$(EMBER_NZBGET_PK_INITSCRIPT))
endef

$(eval $(generic-package))
